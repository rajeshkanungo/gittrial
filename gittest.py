import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
import mysql.connector
from  flask import Flask



def covidapp():
    pranit = mysql.connector.connect(host="1.1.1.121", user="replica", passwd="replica", port="3311",
                                   database="covtrack")
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * from userReportDailySymptoms")
    column_name = [i[0] for i in mycursor.description]

    sym = []
    for db in mycursor:
        s1 = list(db)
        sym.append(s1)
        sym1 = pd.DataFrame(sym)
        sym1.columns = column_name
        t1 = sym1.drop_duplicates(subset='userId', keep='last')
        tb1 = t1[
            ['userId', 'dryCoughCondition', 'feverCondition', 'breathlessnessCondition', 'fatigueMusclePainCondition',
             'lossOfAppetiteCondition']]

    mycursor.execute("SELECT * from userPersonalInfo")
    column_name = [i[0] for i in mycursor.description]

    sym2 = []
    for db1 in mycursor:
        s2 = list(db1)
        sym2.append(s2)
        sym3 = pd.DataFrame(sym2)
        sym3.columns = column_name
        t2 = sym3.drop_duplicates(subset='userId', keep='last')
        t2master = t2.loc[:, ['userId', 'age', 'height', 'weight']]
        # t2master = t2[['userId', 'age','height','weight']]
        t2master['bmi'] = ((t2master['weight'] / t2master['height'] / t2master['height']) * 10000)
        bmi_type = []
        for bmi in t2master['bmi']:
            if 19 < bmi < 25:
                bmi_type.append(1)

            elif 25 < bmi < 30:
                bmi_type.append(2)

            elif bmi > 30:
                bmi_type.append(4)
            else:
                bmi_type.append(3)

        t2master['bmi_category'] = bmi_type
        tb2 = t2master[['userId', 'age', 'bmi_category']]

    mycursor.execute("SELECT * from userMedicalHistory")
    column_name = [i[0] for i in mycursor.description]

    sym4 = []
    for db2 in mycursor:
        s3 = list(db2)
        sym4.append(s3)
        sym5 = pd.DataFrame(sym4)
        sym5.columns = column_name
        t3 = sym5.drop_duplicates(subset='userId', keep='last')
        tb3 = t3[['userId', 'isDiabetic', 'hasHypertension', 'hasCardiovascularDisease', 'hasChronicRespiratoryDisease',
                  'hasKidneyLiverAilmentsORTransplants', 'hasHIV']]

    mycursor.execute("SELECT * from userHealthRiskFactors")
    column_name = [i[0] for i in mycursor.description]

    sym6 = []
    for db3 in mycursor:
        s4 = list(db3)
        sym6.append(s4)
        sym7 = pd.DataFrame(sym6)
        sym7.columns = column_name
        t4 = sym7.drop_duplicates(subset='userId', keep='last')
        tb4 = t4[['userId', 'smoking', 'alcohol']]

    tb_join1 = pd.merge(tb1, tb2, on='userId', how='outer')
    tb_join2 = pd.merge(tb3, tb4, on='userId', how='outer')
    tb_final = pd.merge(tb_join1, tb_join2, on='userId', how='outer')




    t = tb_final.iloc[:, 1:]

    for b in t.columns.drop("age"):
        t[b] = t[b].map(
            {0: 0, 1: 1, 2: 2, 3: 3, 'yes': 1, 'no': 0, 'Yes': 1, 'No': 0, 'YES': 1, 'NO': 0, 'None': 0, "none": 0,
             "Severe": 3, "Mild": 2, "Low": 1, "High": 3,
             "Medium": 2, "severe": 3, "mild": 2, "low": 1, "high": 3, "medium": 2})
        t[b] = t[b].fillna(0)

    age_group = []

    for i in t['age']:
        if 0 < i < 5:
            age_group.append(5)

        elif 5 < i < 15:
            age_group.append(2)

        elif 15 < i < 55:
            age_group.append(1)

        else:
            age_group.append(6)

    t['age'] = age_group

    parameters = []

    for i in range((t.shape[0])):
        parameters.append(list(t.iloc[i, :]))

    weight = [0.03, 0.11, 0.13, 0.02, 0.03, 0.15, 0.07, 0.03, 0.02, 0.1, 0.16, 0.05, 0.04, 0.03, 0.03]

    score = []
    condition = []

    for g in range(len(parameters)):
        weighted_avg = np.average(parameters[g], weights=weight)
        score.append(weighted_avg)

    for h in score:
        if h < 1:
            condition.append("Low")

        elif (1 <= h <= 2):
            condition.append("Moderate")

        else:
            condition.append("Critical")

    tb_final['condition'] = condition
    tb_final['score'] = score
    tb_final.set_index(tb_final['userId'])

    # ML Model Integration

    x_data = t.iloc[:, 0:]
    x_data['score'] = score
    y_data = tb_final['condition']
    model = LogisticRegression(multi_class='ovr')
    model.fit(x_data, y_data)


    tb_final_dict= tb_final.to_json()

    return tb_final_dict